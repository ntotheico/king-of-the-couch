// Minimal Anforderung:
// Es gibt eine Liste von Spielen
// Es wird ein zufälliges Spiel ausgewählt
// Das Spiel wird bei der nächsten Zufallswahl nicht berücksichtigt, bis alle Spiele gespielt wurden / alle gleich oft gespielt wurden

// IDEE: http://dougtesting.net/winwheel/examples/wheel_of_fortune
import { games, player } from './modules/data.js';
import { shuffle, toggleVisibility } from './modules/utilities.js';
import { gameContainer, generatePvPFixture, generate2on2Fixture, generatePartyFixture, clearModalInner } from './modules/fixtures.js';
import { setPvPFinalGameScore, clearGameScore, resetPartyGameScoreCount } from './modules/score';
import { attachTotalScoreLeaderboard } from './modules/leaderboard';

const 
    newRoundBtn = document.querySelector('.new-round'),
    nextGameBtn = document.querySelector('.next-game'),
    endGameBtn = document.querySelector('.end-game'),
    totalScoreWrapper = document.querySelector('#total-score');;
let gamesCopy = [];
let playedGamesCount = 0;
let gameModus = '';

let actualGame = '';

function setPlayedGamesCount() {
    playedGamesCount += 1;
}
function getPlayedGamesCount() {
    return playedGamesCount;
}

function getGameModus(){
    return gameModus;
}

function setGameModus(mode){
    gameModus = mode;
}

function newRound() {
    toggleVisibility(newRoundBtn);
    toggleVisibility(nextGameBtn);
    generateTotalScore(player);
    gamesCopy = games.slice();
    nextGame();
}

function generateTotalScore(player) {
    let totalScoreContainer = [];
    player.forEach((p, i) => {
        totalScoreContainer.push({ name: p.name, score: p.score });
    });
    localStorage.setItem('totalscore', JSON.stringify(totalScoreContainer));
}

function chooseRandomGame() {
    if (gamesCopy.length > 0) {
        shuffle(gamesCopy);
        let game = gamesCopy.pop();
        return game;
    }
    toggleVisibility(newRoundBtn);
    toggleVisibility(nextGameBtn);
    const playedAllGames = {
        title: "Alle Spiele wurden gespielt."
    };
    return playedAllGames;
}

function setActualGame(title) {
    actualGame = title;
}

function getActualGame() {
    return actualGame;
}

function nextGame() {
    const gameTitleContainer = document.querySelector(".game-title");
    const gameTitleExits = gameTitleContainer === null ? false : true;
    const actualTotalScoreContainer = totalScoreWrapper.querySelector('.total-score-leaderboard');
    const actualTotalScoreExits = actualTotalScoreContainer === null ? false : true;
    clearModalInner();

    if (gameTitleExits) {
        gameContainer.removeChild(gameTitleContainer);
    }
    if (actualTotalScoreExits){
        totalScoreWrapper.removeChild(actualTotalScoreContainer);
    }
    let game = chooseRandomGame();
    setGameModus(game.mode);
    setActualGame(game.title);
    setPlayedGamesCount();
    let playerScoreContainer = [];
    player.forEach( (p, i) => {
        playerScoreContainer.push({name: p.name, score: 0});
    });
    localStorage.setItem(game.title, JSON.stringify(playerScoreContainer));
    let gameTitle = `<h1 class=>${game.title}</h1>`;
    gameContainer.innerHTML = gameTitle;
    gameContainer.classList.add('game-is-active');

    switch (game.mode) {
        case "Party":
            generatePartyFixture(player);
            break;
        case "PvP":
            generatePvPFixture(player);
            break;
        case "2on2":
            generate2on2Fixture(player);
            break;
        default:
            console.log("default case");
            break;
    }
}

function endGame() {
    console.log('%cendGame()', 'background-color: red; color: white; padding: 10px;')
    let mode = getGameModus();
    if (mode === 'PvP') {
        setPvPFinalGameScore();
        let leaderboard = gameContainer.querySelector('.leaderboard');
        gameContainer.removeChild(leaderboard);
    }
    let totalScore = attachTotalScoreLeaderboard();
    let fixture = document.querySelector('.fixture-table');

    gameContainer.classList.remove('game-is-active');
    fixture.parentNode.removeChild(fixture);
    
    totalScoreWrapper.appendChild(totalScore);
    toggleVisibility(nextGameBtn);
    clearGameScore(getActualGame());
    resetPartyGameScoreCount();
    toggleVisibility(this);
    document.querySelector('h1').style.display = 'none';
}

localStorage.clear();
toggleVisibility(nextGameBtn);
toggleVisibility(endGameBtn);
nextGameBtn.addEventListener('click', nextGame);
newRoundBtn.addEventListener('click', newRound);
endGameBtn.addEventListener('click', endGame);

export { getActualGame, nextGameBtn, endGameBtn, getPlayedGamesCount };