const games = [
    // { title: "Crawl", mode: "Party" },
    { title: "Trials Fusion", mode: "Party" },
    { title: "Niddhogg", mode: "PvP" },
    { title: "Laser League", mode: "2on2" },
    { title: "Gang Beasts", mode: "Party" },
    // { title: "Wizards Tourney", mode: "Party" },
    // { title: "Duck Game", mode: "Party" },
    { title: "Nidhogg 2", mode: "PvP" },
    { title: "Wissen ist Macht", mode: "Party" },
    { title: "Wissen ist Macht Dekaden", mode: "Party" },
    { title: "Brawlhalla", mode: "Party" },
    // { title: "DE Formers", mode: "Party" },
    // { title: "Jump Stars", mode: "Party" },
    { title: "Broforce", mode: "Party" },
    { title: "Starwhal", mode: "Party" },
    { title: "Overcooked", mode: "2on2" },
    { title: "Tetris Ultimate", mode: "Party" },
    { title: "Trivial Pursuit", mode: "Party" },
    { title: "Tricky Towers", mode: "Party" },
    { title: "Stikbold!", mode: "Party" },
    { title: "Towerfall Ascension", mode: "Party" },
    { title: "Push Me Pull You", mode: "2on2" },
    { title: "Sports Friends - Super Pole Riders", mode: "PvP" },
    { title: "Sports Friends - Hokra", mode: "2on2" },
    { title: "Sports Friends - Barabariball", mode: "2on2" },
    { title: "Sports Friends - Flop", mode: "PvP" },
    { title: "Sports Friends - Get on Top", mode: "PvP" },
    { title: "Videoball", mode: "PvP" },
    { title: "Disc Jam", mode: "PvP" },
    { title: "Divekick", mode: "PvP" },
    { title: "Rocket League", mode: "PvP" },
    { title: "Rocket League", mode: "2on2" },
    // { title: "In Space we Brawl", mode: "Party" },
    { title: "Worms Battleground", mode: "Party" },
    // { title: "Extreme Exorcism", mode: "Party" },
    // { title: "Roarr!", mode: "Party" },
    { title: "SpeedRunners", mode: "Party" },
    { title: "Jackbox", mode: "Party" }
];

const player = [
    { name: "Nico", score: 0 },
    { name: "Malte", score: 0 },
    { name: "Benny", score: 0 },
    { name: "Marc", score: 0 }
];

export { games, player };