import { player } from './data.js';
import { shuffle, toggleVisibility } from './utilities.js';
import { nextGameBtn } from '../index.js';
import { setGames2Play } from './game';
import { setPvPGameScore, setPartyScore, setTwoOnTwoScore } from './score';
import { attachGameLeaderboard } from './leaderboard';

const gameContainer = document.querySelector("#game");
const playerCount = player.length;
const modalInner = document.querySelector('.modal-inner');
const modalOuter = document.querySelector('.modal-outer');

let parentTrigger;

function getPartentTrigger() {
    return parentTrigger;
}

function setParentTrigger(trigger) {
    parentTrigger = trigger;
}

function fixturePreparation() {
    toggleVisibility(nextGameBtn);
}

function generatePvPFixture(player) {
    let roundsToPlay = playerCount - 1;

    let matches = [];
    for (let i = 0; i < roundsToPlay; i++) {
        for (let k = i + 1; k < playerCount; k++) {
            matches.push([player[i], player[k]]);
            setGames2Play();
        }
    }
    shuffle(matches);
    attachFixture(matches, 'PvP');
    fixturePreparation();
    
}

function generatePartyFixture(player) {
    attachFixture(player, 'Party');
    fixturePreparation();
}

function generate2on2Fixture(player) {
    let teams = [];
    let teamCount = playerCount / 2;
    shuffle(player);

    for (let i = 0; i < teamCount; i++) {
        teams[i] = `<span class="two-on-two-player" data-player="${player[i].name}">${player[i].name}</span> und <span class="two-on-two-player" data-player="${player[i + 2].name}">${player[i + 2].name}</span>`;
    }
    attachFixture(teams, '2on2');
    fixturePreparation();
}

function attachFixture(array, mode) {
    let playerTable = document.createElement('table');
    playerTable.classList.add('fixture-table');
    let playerTableBody = document.createElement('tbody');
    playerTable.appendChild(playerTableBody);

    if (mode === '2on2') {
        playerTableBody.insertAdjacentHTML('beforeend', `<tr><td class="two-on-two-team">${array[0]}</td><td>vs.</td><td class="two-on-two-team">${array[1]}</td></tr>`);
    } else {
        array.forEach(element => {
            switch (mode) {
                case 'PvP':
                    playerTableBody.insertAdjacentHTML('beforeend', `
                        <tr class="fixture">
                            <td class="player" data-player="${element[0].name}">${element[0].name}</td>
                            <td>vs</td>
                            <td class="player" data-player="${element[1].name}">${element[1].name}</td>
                        </tr>
                    `);
                    break;
                case 'Party':
                    playerTableBody.insertAdjacentHTML('beforeend', `<tr><td class="party-player" data-player="${element.name}">${element.name}</td></tr>`);
                    break;
                default:
                    console.log('Default Case');
                    break;
            }
        });
    }
    gameContainer.appendChild(playerTable);
    if (mode === 'PvP') {
        const leaderBoard = attachGameLeaderboard();
        const fixtureTrigger = document.querySelectorAll('.fixture');
        fixtureTrigger.forEach(handleFixtureTrigger);
        gameContainer.appendChild(leaderBoard);
    }
    if (mode === 'Party') {
        createPartyModalContent(playerTable);
        playerTable.addEventListener('click', openPartyModal);
    }
    if (mode === '2on2') {
        const twoOntwoTrigger = playerTable.querySelectorAll('.two-on-two-team');
        twoOntwoTrigger.forEach(handleTwoOnTwoTrigger);
    }
}

function updateFixture(winner) {
    let fixture = getPartentTrigger();
    let player = fixture.querySelectorAll('.player');
    let fixtureWinner = String(winner);

    player.forEach(p => {
        if (p.dataset.player === fixtureWinner) {
            p.classList.add('winner');
        } else {
            p.classList.add('loser');
        }
    });
}

function openFixtureModal(event) {
    const trigger = event.currentTarget;
    const player = trigger.querySelectorAll('.player');
    setParentTrigger(trigger);
    modalInner.innerHTML = `
        <span class="actual-fixture-player" data-player="${player[0].innerText}">${player[0].innerText}</span>
        <span>vs.</span>
        <span class="actual-fixture-player" data-player="${player[1].innerText}">${player[1].innerText}</span>
    `;

    const actualFixturePlayer = modalInner.querySelectorAll('.actual-fixture-player');
    
    actualFixturePlayer.forEach(handlePvPPlayerTrigger);
    // Show Modal
    modalOuter.classList.add('open');
}
function createPartyModalContent(playerTable){
    const table = playerTable;
    const player = table.querySelectorAll('.party-player');

    modalInner.classList.add('grid');
    modalInner.insertAdjacentHTML('afterbegin', `<h2 class="party-play__headline">Wer hat gewonnen?</h2>`);
    player.forEach( p => {
        modalInner.insertAdjacentHTML('beforeend', `<span class="party-player-trigger" data-player="${p.dataset.player}">${p.dataset.player}</span>`);
    })
    const actualFixturePlayer = modalInner.querySelectorAll('.party-player-trigger');
    actualFixturePlayer.forEach(handlePartyPlayerTrigger)  

}

function openPartyModal() {
  
    modalOuter.classList.add('open');

}

function closeModal() {
    modalOuter.classList.remove('open');
}

function clearModalInner(){
    modalInner.innerHTML ="";
    modalInner.classList.remove('grid');
}

modalOuter.addEventListener('click', function (event) {
    const isOutside = !event.target.closest('.modal-inner');
    if (isOutside) closeModal();
})

function handleFixtureTrigger(fixtureTrigger){
    fixtureTrigger.addEventListener('click', openFixtureModal);
};

function handlePartyPlayerTrigger(partyTrigger) {
    partyTrigger.addEventListener('click', setPartyScore);
}

function handlePvPPlayerTrigger(playerTrigger) {
    playerTrigger.addEventListener('click', setPvPGameScore);
}

function removeHandleFixtureTrigger() {
    parentTrigger.removeEventListener('click', openFixtureModal);
}

function handleTwoOnTwoTrigger(twoOntwoTrigger){
    twoOntwoTrigger.addEventListener('click', setTwoOnTwoScore);
}

export { gameContainer, generatePvPFixture, generate2on2Fixture, generatePartyFixture , removeHandleFixtureTrigger, updateFixture, closeModal, clearModalInner };