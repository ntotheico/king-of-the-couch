import { getActualGame, getPlayedGamesCount } from '../index.js';
import { getTotalScore } from './score';

function attachGameLeaderboard() {
    let gameLeaderboard = document.createElement('table');
    gameLeaderboard.classList.add('leaderboard');
    let gameLeaderboadBody = document.createElement('tbody');
    gameLeaderboard.appendChild(gameLeaderboadBody);
    
    let game = getActualGame();
    let gameScore = JSON.parse(localStorage.getItem(game));
    gameScore.sort(function (a, b) {
        return a.score - b.score;
    });

    gameScore.forEach(player => {
        gameLeaderboadBody.insertAdjacentHTML('afterbegin', `
            <tr><td class="player--name">${player.name}</td><td class="player--score">${player.score}</td></tr>
        `);
    })
    return gameLeaderboard;
}

function attachTotalScoreLeaderboard() {
    console.log('attachTotalScoreLeaderboard()');
    let totalScoreLeaderboard = document.createElement('table');
    totalScoreLeaderboard.classList.add('total-score-leaderboard');

    let totalScoreCaption = document.createElement('caption');
    let gamesPlayed = getPlayedGamesCount();
    totalScoreCaption.textContent = `Gesamtpunkte nach ${gamesPlayed} gespielten Spielen`;
    totalScoreLeaderboard.appendChild(totalScoreCaption);

    let totalScoreLeaderboardBody = document.createElement('tbody');
    totalScoreLeaderboard.appendChild(totalScoreLeaderboardBody);

    let totalScore = getTotalScore();
    totalScore.sort(function (a, b) {
        return a.score - b.score;
    });

    totalScore.forEach(player => {
        totalScoreLeaderboardBody.insertAdjacentHTML('afterbegin', `
            <tr><td class="player--name">${player.name}</td><td class="player--score">${player.score}</td></tr>
        `);
    })
    return totalScoreLeaderboard;
}

export { attachGameLeaderboard, attachTotalScoreLeaderboard };