import { getActualGame, endGameBtn } from '../index.js';
import { removeHandleFixtureTrigger , updateFixture, closeModal, gameContainer } from './fixtures';
import { attachGameLeaderboard } from './leaderboard';
import { setGamesPlayed, allGamesPlayed } from './game';
import { toggleVisibility } from './utilities';

let partyGameScoreCount = 0;

function setPartyGameScoreCount() {
    partyGameScoreCount += 1;
}

function getPartyGameScoreCount(){
    return partyGameScoreCount;
}
function resetPartyGameScoreCount() {
    partyGameScoreCount = 0;
}

function sortGameScore(gameScore) {
    let sortedGameScore = gameScore.sort(function (a, b) {
        return b.score - a.score;
    });
    return sortedGameScore;
}

function getGameScore(game) {
    return sortGameScore(JSON.parse(localStorage.getItem(game)));
}

function setPvPGameScore(event) {
    let game = getActualGame();
    let gameScore = getGameScore(game);
    let player = this.dataset.player;

    updatePlayerScore(gameScore, player);
    localStorage.setItem(game, JSON.stringify(gameScore));

    removeHandleFixtureTrigger();
    updateFixture(player);
    setGamesPlayed();
    closeModal();
    if (allGamesPlayed()) {
        toggleVisibility(endGameBtn);
    } 
    let leaderBoard = gameContainer.querySelector('.leaderboard');
    gameContainer.removeChild(leaderBoard);
    gameContainer.appendChild(attachGameLeaderboard());
}
function setPartyScore(){
    setPartyGameScoreCount();
    this.removeEventListener('click', setPartyScore);
    let rank = getPartyGameScoreCount();
    if (rank === 1) {
        toggleVisibility(endGameBtn);
    }
    if (rank < 4) {
        let game = getActualGame();
        let gameScore = getGameScore(game);
        const element = this;
        element.classList.add('in-ranking');
        let headline = element.closest('.modal-inner').querySelector('.party-play__headline');
        console.log(headline);
        const player = this.dataset.player;
        updatePlayerRank(gameScore, player, rank);
        localStorage.setItem(game, JSON.stringify(gameScore));
        updateTotalScore({ name: player, rank: rank });
        let headlineText = '';
        switch(rank){
            case 1:
                element.classList.add('gold');
                headlineText = 'Wer ist zweiter?';
                break;
            case 2:
                element.classList.add('silver');
                headlineText = 'Wer ist dritter?';
                break;
            case 3:
                element.classList.add('bronze');
                headlineText = 'Alle Platzierungen wurden vergeben';
                break;
            default:
                console.log('');    
                break;
        }
        headline.innerText = headlineText; 
    } else {
        //closeModal();
        return
    }
}

function setTwoOnTwoScore() {
    this.classList.add('in-ranking', 'gold');
    let twoOnTwoTrigger = document.querySelectorAll('.two-on-two-team');
    twoOnTwoTrigger.forEach( item => item.removeEventListener('click', setTwoOnTwoScore));
    let winner = this.querySelectorAll('.two-on-two-player');
    winner.forEach( player => {
        let name = player.dataset.player;
        let rank = 2;
        updateTotalScore({name: name, rank: rank});
    });
    toggleVisibility(endGameBtn);
}

function setPvPFinalGameScore() {
    let game = getActualGame();
    let gameScore = getGameScore(game);

    let ranked = gameScore.map(function (item, i) {
        if (i > 0) {
            //Get our previous list item
            var prevItem = gameScore[i - 1];
            if (prevItem.score == item.score) {
                //Same score = same rank
                item.rank = prevItem.rank;
            } else {
                //Not the same score, give em the current iterated index + 1
                item.rank = i + 1;
            }
        } else {
            //First item takes the rank 1 spot
            item.rank = 1;
        }

        return item;
    });

    let winnerCounter = 0;
    ranked.forEach(player => {
        if( player.rank < 4 && winnerCounter < 4) {
            updateTotalScore(player);
            winnerCounter++;
        }
    });
}

function updatePlayerScore(updateScore, player) {
    for (let i in updateScore) {
        if (updateScore[i].name == player) {
            updateScore[i].score += 1;
            break; //Stop this loop, we found it!
        }
    }
}

function updatePlayerRank(updateScore, player, rank) {
    for (let i in updateScore) {
        if (updateScore[i].name == player) {
            updateScore[i].rank = rank;
            break; //Stop this loop, we found it!
        }
    }
}

function updateTotalScore({ rank, name }) {
    let totalScore = getTotalScore();
    let playerGameScore;
    switch (rank) {
        case 1:
            playerGameScore = 3;
            break;
        case 2:
            playerGameScore = 2;
            break;
        case 3:
            playerGameScore = 1;
            break;
        default:
            playerGameScore = 0;
            break;
    }
    for ( let i in totalScore ) {
        if (totalScore[i].name == name) {
            totalScore[i].score += playerGameScore;
            break; //Stop this loop, we found it!
        }
    }

    localStorage.setItem('totalscore', JSON.stringify(totalScore));
}

function clearGameScore(key) {
    localStorage.removeItem(key);
}

function getTotalScore() {
    // evlt umstellen auf sortScore
    console.log('getTotalScore()');
    return sortGameScore(JSON.parse(localStorage.getItem('totalscore')));
}

export { getGameScore, setPvPGameScore, setPvPFinalGameScore, getTotalScore, updateTotalScore, clearGameScore, setPartyScore, resetPartyGameScoreCount, setTwoOnTwoScore };