let games2Play = 0;
let gamesPlayed = 0;

function setGames2Play() {
    games2Play = games2Play + 1;
}

function getGames2Play() {
    return games2Play;
}

function setGamesPlayed() {
    gamesPlayed = gamesPlayed + 1;
}

function getGamesPlayed() {
    return gamesPlayed;
}

function allGamesPlayed() {
    return (getGamesPlayed() === getGames2Play() ? true : false);
}

export { setGames2Play, setGamesPlayed, allGamesPlayed };